
resource "aws_lambda_function" "api" {
  filename      = var.lambda_file_path
  function_name = var.function_name
  role          = var.lambda_role_arn
  handler       = var.handler


  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256(var.lambda_file_path)

  runtime = var.runtime
  timeout = 150
  environment {
    variables = var.lambda_env
  }
  publish=true
}
resource "aws_lambda_alias" "api_alias" {
  name             = "developement"
  description      = "Alias for developement"
  function_name    = aws_lambda_function.api.arn
  function_version = "$LATEST"
  depends_on = [aws_lambda_function.api]
}
